
# 1.Write a program that calculates the factorial of a given number using a loop.
def factorial(n):
    fact=1
    for i in range(1,number+1):
        fact*=i
    return fact
number=5
print(f"The factorial of {number} is:",factorial(number))
# 2. Write a function that takes a string as input and returns the count of uppercase and lowercase letters separately
def check_upper_lower(string):       # Version 1
    ascii_str=string.encode("ascii")
    upper=0
    lower=0
    for i in ascii_str:
        if(i>=65 and i<=90):
            upper+=1
        if(i>=97 and i<=122):
            lower+=1
    return upper,lower
string="HeLlo"  
print(check_upper_lower(string)) 

def check_isupper_islower(string1):    #Version 2
    upper=0
    lower=0
    for i in string1:
        if(i.isupper()):
            upper+=1
        if(i.islower()):
            lower+=1   
    return upper, lower
string1="HeLlo"
print("Upper, Lower", check_isupper_islower(string1))

#3. Write a program that takes a word as input and determines whether it is a palindrome (reads the same backward as forward).

def check_palind(word):
    check=0
    for i in range(len(word)//2):
        if(word[i]==word[-1-i]):
            check+=1
    if(check==len(word)//2):
        return True  
    else:
        return False    
str1="abcdcba"
print(f"Is the '{str1}' word palindrome? ",check_palind(str1))

# 4. Write a program that takes a number as input and calculates
#  the sum of the squares of all numbers from 1 to that number.
def sum_of_squares(num):           # Version 1
    sum=0
    sq=[x**2 for x in range(num+1)]
    for i in sq:
        sum+=i
    return sum     # or sum(sq)
num=3
print(f"The sum of squares from 1 to {num}  is:",sum_of_squares(num))

def sum_of_squares2(num1):           # Version 2
    total=0
    [total := total + x**2 for x in range(num1+1)]
    return total
num1=4
print(f"The sum of squares from 1 to {num1}  is:",sum_of_squares2(num1))

# 5. Write a program that takes a string as input and prints the frequency of each character in the string.
str2="Hello"               # Version 1 list solution
list1=list(str2)
set1=set(list1)
for i in set1:
    x=list1.count(i)
    print(i,x)

string="Hello"             # Version 2 Dictionary solution
d={}
for i in string:
    if not i in d:
        d[i]=1
    else:
        d[i]+=1
print(d)      

#6. Write a program that takes a number as input and checks whether it is a perfect number 
# (a positive integer that is equal to the sum of its proper divisors, excluding itself).  

def check_isperfect_num(p_num):
    sum1=0
    for i in range(1,p_num+1):
        if(p_num%i==0):
            sum1+=i
    if(p_num==sum1):
        return True
    else:
        return False        
p_num=10        
print(f"The number {p_num} is perfect: ",check_isperfect_num(p_num))           

# 7. Write a program using nested loops to print the following pattern:
for s in range(6):
    for t in range(s):
        print(s, end=" ")
    print('')     

# 8. Generate a random number between 1 and 50. Ask the user to guess the number. 
# Provide hints such as "Too high" or "Too low" until they guess correctly.
import random
x = str(random.randint(0,50))
print(x)
while True:
    inp=input("Please enter a number between 1 and 50 -> ")
    if(inp<x):
        print("Too low")
    if(inp>x):
        print("Too high")
    if (inp==x):
        print("You guessed !!!!!")
        break
# 9. Write a program that takes a number as input and checks whether it is an Armstrong number 
# (the sum of its own digits each raised to the power of the number of digits). 
def check_isArmstrong(arm_num):
    inp1=arm_num
    inp2=arm_num
    sum3=0
    count=0
    while(inp1!=0):
         inp1=inp1//10
         count+=1
    print(count)
    while(inp2!=0):
        rem=inp2%10
        sum3+=rem**count
        inp2=inp2//10
    if(arm_num==sum3): 
        print(f"The number {arm_num} is Armstrong") 
    else:
        print(f"The number {arm_num} isn't Armstrong")
arm_num =int(input("Input a number to check whether it is an Armstrong number or not: "))  
check_isArmstrong(arm_num)

# 10. Write a program that generates the Fibonacci sequence up to a specified number of terms. 
# The Fibonacci sequence starts with 0 and 1, and each subsequent term is the sum of the two preceding ones.  
def fibonacci(f_num):     # Version 1
    FibArray = [0,1]
    if(f_num<=0):
        return "Incorrect Value"
    if(f_num==1):
        return FibArray[0]
    if(f_num==2):
        return FibArray 
    if(f_num>2):
        f=2
        while(f!=f_num):
            FibArray.append(FibArray[f - 1] + FibArray[f - 2])
            f+=1
    return FibArray
 
print("Fibonacci sequence list: ",fibonacci(5))

fib = [0, 1]           # Version 2
for i in range(2,5):
    fib.append(fib[i-1]+fib[i-2])
print("Fibonacci sequence list: ",fib)    

        
    

        





    



